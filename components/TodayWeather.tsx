import React from 'react'
import {MdLocationOn} from 'react-icons/md'
import {FiSun} from 'react-icons/fi'

const TodayWeather = () => {
  return (
    <div className="today">
      <div className="today__header">
        <h1><MdLocationOn/> Mérida, Mx</h1>
      </div>

      <div className="today__main">
        <FiSun/>

        <div className="today__temperature">
          <div className="today__temperatureMain">
            <h1>32*C</h1>
          </div>
          <div className="today_minMax">
            <span>min: 30*C</span>
            <span>max: 40*C</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TodayWeather
