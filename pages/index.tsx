import Header from "@components/Header";
import Footer from "@components/Footer";
import TodayWeather from "@components/TodayWeather";
import styles from "../styles/Home.module.scss";

export default function Home() {
  return (
    <div>
      <Header />

      <div className="container">

        <div className="row">

         <div className="col-8">
           
           <TodayWeather />

         </div>
         <div className="col-4">
           Week
         </div>

        </div>
      </div>

      <Footer/>
    </div>
  );
}
